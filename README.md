# jmeter-performance-testing

## Prerequisites

- docker
- docker-compose
- git
- JMeter 5.2
- jmeter-plugins : https://jmeter-plugins.org/install/Install/
- SteppingThreadGroup : https://jmeter-plugins.org/wiki/SteppingThreadGroup/ jpgc-casutg

## Monitoring

In order to monitor you virtual machines and your containers, you will have to deploy some containers, like cAdvisor, Node exporter etc.

On your vsprd-1 vm :

```
docker-compose -f monitoring/docker-compose-vsprd-1.yml up --detach
```

On your vsprd-2 vm :

```
docker-compose -f monitoring/docker-compose-vsprd-2.yml up --detach
```

## Gitlab Runner

For this lab, you will need 3 Gitlab Runners. For more informations, follow the instructions on net-security website.

## Run performance test through Gitlab Pipeline

With this project, you will be able to run multiple types of performance testing load :
- To run an unit test, run a pipeline with variable TYPE_TEST and value performance_testing_unit
- To run an rated test, run a pipeline with variable TYPE_TEST and value performance_testing_nominal
- To run a 300% rated test, run a pipeline with variable TYPE_TEST and value performance_testing_palier
