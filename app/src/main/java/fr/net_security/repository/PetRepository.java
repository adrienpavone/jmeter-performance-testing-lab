package fr.net_security.repository;

import fr.net_security.model.Pet;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PetRepository extends JpaRepository<Pet, Integer> {
    List<Pet> findByStatus(String status, Pageable page);

    List<Pet> findByStatusAndCategory(String status, String category, Pageable pageable);
}
