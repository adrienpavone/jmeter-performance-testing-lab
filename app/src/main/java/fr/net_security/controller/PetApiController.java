package fr.net_security.controller;

import fr.net_security.exception.PetNotFoundException;
import fr.net_security.model.Pet;
import fr.net_security.repository.PetRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v2/pets")
public class PetApiController {
    private static final Logger log = LoggerFactory.getLogger(PetApiController.class);

    @Autowired
    private PetRepository petRepository;

    @GetMapping
    List<Pet> all(@RequestParam(required = false) String status, @RequestParam(required = false) Integer page) {
        Integer pageNumber = (page == null) ? 0 : page ;
        Pageable pageable = PageRequest.of(pageNumber, 50, Sort.by("id"));
        if (status != null)
            return petRepository.findByStatus(status, pageable);
        else
            return petRepository.findAll(pageable).getContent();
    }

    @PostMapping
    Pet newPet(@RequestBody Pet newPet) {
        return petRepository.save(newPet);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<?> deletePet(@PathVariable int id) throws PetNotFoundException {
        Pet pet = petRepository.findById(id).orElseThrow(() -> new PetNotFoundException("Pet id " + id + " not found"));
        petRepository.delete(pet);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/{id}")
    Pet updatePet(@PathVariable int id, @RequestBody Pet updatePet) throws PetNotFoundException {
        Pet pet = petRepository.findById(id).orElseThrow(() -> new PetNotFoundException("Pet id " + id + " not found"));
        updatePet.setId(pet.getId());
        petRepository.save(updatePet);

        return updatePet;
    }

    @GetMapping("/{id}")
    Pet onePet(@PathVariable int id) throws PetNotFoundException {
        return petRepository.findById(id).orElseThrow(() -> new PetNotFoundException("Pet id " + id + " not found"));
    }
}
