package fr.net_security.controller;

import fr.net_security.exception.PetNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ExceptionHandlingController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(PetNotFoundException.class)
    public ResponseEntity notFound(PetNotFoundException ex) {
        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.put("Content-Type", Collections.singletonList("application/json;charset=UTF-8"));
        Map<String, String> map = new HashMap<>();
        map.put("message", ex.getMessage());
        return new ResponseEntity<>(map, headers, HttpStatus.NOT_FOUND);
    }
}
